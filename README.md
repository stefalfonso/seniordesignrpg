# SeniorDesignRPG

## Software ##
Unity5 (5.3.1f1)

Oculus Runtime and SDK
https://developer.oculus.com/downloads/

Kinect for Windows v2.0
https://www.microsoft.com/en-us/download/details.aspx?id=44561

Kinect for Unity v2 Samples
https://www.assetstore.unity3d.com/en/#!/content/18708

Oculus for Unity Example
https://developer.oculus.com/blog/unitys-ui-system-in-vr/ 

## Hardware ##
Kinect for Windows v2

Oculus Rift DK2
