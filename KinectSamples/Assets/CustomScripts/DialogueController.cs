﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class DialogueController : MonoBehaviour {

    [Tooltip("File with dialogue to read from (in format imgpath+speaker: text.")]
    public TextAsset txtFile = new TextAsset();


    UnityEngine.UI.Text text;
    UnityEngine.UI.Image avatar;
    string[] dialogLines;
    int lineNum;
    ButtonGestureListener gestureListener; // TODO: make a generic gesture listener or menu gesture listener
    float timeBetweenSteps = .2f;
    float lastStep = 0;

    [Tooltip("name of level to load upon completion of this dialogue")]
    public string levelLoad = null;

    private Dictionary<string, Sprite> nameToAvatar;



    // Use this for initialization
    void Start () {
       nameToAvatar  = new Dictionary<string, Sprite>
        {
            // TODO
              {"You",  Sprite.Create(Resources.Load<Texture2D>("Artwork/avesMainCharFlat"), new Rect(0, 0, 400, 400), new Vector2(400, 400)) },
            {"Raven",  Sprite.Create(Resources.Load<Texture2D>("Artwork/avesMainCharFlat"), new Rect(0, 0, 400, 400), new Vector2(400, 400)) },
            {"Elred", Sprite.Create(Resources.Load<Texture2D>("Artwork/elredavatar"), new Rect(0, 0, 400, 400), new Vector2(400, 400)) }

        };
            gestureListener = ButtonGestureListener.Instance;
        print("start");
        avatar = GameObject.Find("Dialog/Avatar").GetComponent<UnityEngine.UI.Image>();
        text = GameObject.Find("Dialog/Panel/Text").GetComponent<UnityEngine.UI.Text>();
        if (txtFile)
        {
            dialogLines = txtFile.text.Split('\n');
        }
        else
        {
            dialogLines = new string[1];
            dialogLines[0] = "img.jpg+You: This is the default text, meaning there was no provided txtFile.";
            print("dialogueLines[0] is " + dialogLines[0]);
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (gestureListener)
        {
            if (gestureListener.IsSwipeLeft() || gestureListener.IsSwipeRight() || Input.GetKey(KeyCode.RightArrow))
            {
                // slow down input
                if (Time.time - lastStep > timeBetweenSteps)
                {
                    print("time " + Time.time);
                    lastStep = Time.time;
                    lineNum++;
                }
            }
        } else
        {
            print("gesture listener not initialized");
            if (Input.GetKey(KeyCode.RightArrow))
            {
                // slow down input
                if (Time.time - lastStep > timeBetweenSteps)
                {
                    print("time " + Time.time);
                    lastStep = Time.time;
                    lineNum++;
                }

            }
        }

        // can this ever happen?..
        if (lineNum < 0)
        {
            lineNum = 0;
        }

        if (lineNum >= dialogLines.Length)
        {
            if (levelLoad != null)
            {
                SceneManager.LoadScene(levelLoad);
               // lineNum = 0; // in case scene doesn't load
                print("scene should have loaded");
            } else
            {
                
            }
        }

        // format is imagepath+Speaker: dialog
        if (dialogLines.Length <= lineNum || lineNum < 0)
        {
            return;
        }

        string[] dialogSplit = dialogLines[lineNum].Split(':');

        if (dialogSplit == null || dialogSplit.Length < 2)
        {
            return;
        }

        print("dialogSplit[0] " + dialogSplit[0]);
        print("mapsize " + nameToAvatar.Keys.Count);
    
        Sprite avatarSprite = nameToAvatar[dialogSplit[0].Trim()];
        if (avatarSprite)
        {
            avatar.overrideSprite = avatarSprite;
            avatar.sprite = avatarSprite;
        } else
        {
            print("couldn't load sprite");
            //print("sprite: " + avatarSprite);
            //print("nameToAvatar[Elred] " + nameToAvatar["Elred"]);
        }
        text.text = dialogSplit[1];
	}
}
