﻿using UnityEngine;
using System.Collections;
using System;
//using Windows.Kinect;

public class FlyGestureListener : MonoBehaviour, KinectGestures.GestureListenerInterface
{
    [Tooltip("GUI-Text to display gesture-listener messages and gesture information.")]
    public GUIText gestureInfo;
    public UnityEngine.UI.Text gestureDebug;

    // singleton instance of the class
    private static FlyGestureListener instance = null;

    // internal variables to track if progress message has been displayed
    private bool progressDisplayed;
    private float progressGestureTime;

    // whether the needed gesture has been detected or not
    private bool swipe;
    private bool tiltLeft;
    private bool tiltRight;
    private bool dive;
    private bool flap;
    private bool upFlap; // upstroke flap
    private bool glide;
    private long lastGlide = 0; // when was last T-pose detected?

    private float degreesLeft;
    private float degreesRight;
    private Vector3 flapForce;
    private float diveAngle;

    private float parallelSurfaceArea;


    /// <summary>
    /// Gets the singleton FlyGestureListener instance.
    /// </summary>
    /// <value>The FlyGestureListener instance.</value>
    public static FlyGestureListener Instance
    {
        get
        {
            return instance;
        }
    }

    /// <summary>
    /// Determines whether swipe left is detected.
    /// </summary>
    /// <returns><c>true</c> if swipe left is detected; otherwise, <c>false</c>.</returns>
    public bool IsTiltLeft()
    {
        if (tiltLeft)
        {
            tiltLeft = false;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Determines whether dive is detected.
    /// </summary>
    /// <returns><c>true</c> if swipe left is detected; otherwise, <c>false</c>.</returns>
    public bool IsDive()
    {
        if (dive)
        {
            dive = false;
            return true;
        }

        return false;
    }

    public bool isSwipe()
    {
        if(swipe)
        {
            swipe = false;
            return true;
        }
        return false;
    }

    /// <summary>
    /// Determines whether flap is detected.
    /// </summary>
    /// <returns><c>true</c> if swipe left is detected; otherwise, <c>false</c>.</returns>
    public bool IsFlap()
    {
        if (flap)
        {
            flap = false;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Determines whether up flap is detected.
    /// </summary>
    /// <returns><c>true</c> if swipe left is detected; otherwise, <c>false</c>.</returns>
    public bool IsUpFlap()
    {
        if (upFlap)
        {
            upFlap = false;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Determines whether swipe right is detected.
    /// </summary>
    /// <returns><c>true</c> if swipe right is detected; otherwise, <c>false</c>.</returns>
    public bool IsTiltRight()
    {
        if (tiltRight)
        {
            tiltRight = false;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Determines whether glide is detected. If glide was detected within the last half second, this returns true.
    /// </summary>
    /// <returns><c>true</c> if swipe up is detected; otherwise, <c>false</c>.</returns>
    public bool IsGlide()
    {
       // print("check if glide. Last glide is " + lastGlide);
      //  print("time now is " + DateTime.UtcNow.Ticks);
        if (glide || tiltLeft || tiltRight || flap || dive) // not a strict glide, but all of these move forward
        {
            glide = false;
            lastGlide = DateTime.UtcNow.Ticks;
            print("set last glide " + lastGlide);
            return true;
        } else if (Math.Abs(DateTime.UtcNow.Ticks - lastGlide) < 10000*1500) // check every 1.5 second (10k = millescond)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Determines whether glide is detected.
    /// </summary>
    /// <returns><c>true</c> if swipe up is detected; otherwise, <c>false</c>.</returns>
    public bool IsGlideStrict()
    {
        if (glide)
        {
            glide = false;
            return true;
        }

        return false;
    }

    public float getDegreesRight()
    {
        return degreesRight;
    }

    public float getDegreesLeft()
    {
        return degreesLeft;
    }

    public float getDiveAngle()
    {
        return diveAngle;
    }


    public Vector3 getFlapForce()
    {
        print("flap force is " + flapForce);
        // estimate lift force by 1/2 * p * C * A * v^2
        /* float surfaceArea = 2; // TODO: experiment with this value/get it from gesture info
         float airDensity = 1.225f;
         float liftCoefficient = 100; // TODO
         return (float) .5*airDensity*flapVelocity*flapVelocity*surfaceArea * liftCoefficient;*/
        return flapForce;

        // TODO: thrust force too
    }

    public float getParallelSurfaceArea()
    {
        return parallelSurfaceArea;
    }

    /// <summary>
    /// Invoked when a new user is detected. Here you can start gesture tracking by invoking KinectManager.DetectGesture()-function.
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <param name="userIndex">User index</param>
    public void UserDetected(long userId, int userIndex)
    {
        // the gestures are allowed for the primary user only
        KinectManager manager = KinectManager.Instance;
        if (!manager || (userId != manager.GetPrimaryUserID()))
            return;

        // detect these user specific gestures TODO!!!
        manager.DetectGesture(userId, KinectGestures.Gestures.LeanLeft);
        manager.DetectGesture(userId, KinectGestures.Gestures.LeanRight);
        manager.DetectGesture(userId, KinectGestures.Gestures.Glide);
        manager.DetectGesture(userId, KinectGestures.Gestures.Flap);
       // manager.DetectGesture(userId, KinectGestures.Gestures.UpFlap);
        manager.DetectGesture(userId, KinectGestures.Gestures.Dive);
        manager.DetectGesture(userId, KinectGestures.Gestures.SwipeLeft);
        manager.DetectGesture(userId, KinectGestures.Gestures.SwipeRight);



        if (gestureInfo != null)
        {
            gestureInfo.GetComponent<GUIText>().text = "Lean left, lean right, and keep arms up to glide.";
        }
    }

    /// <summary>
    /// Invoked when a user gets lost. All tracked gestures for this user are cleared automatically.
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <param name="userIndex">User index</param>
    public void UserLost(long userId, int userIndex)
    {
        // the gestures are allowed for the primary user only
        KinectManager manager = KinectManager.Instance;
        if (!manager || (userId != manager.GetPrimaryUserID()))
            return;

        if (gestureInfo != null)
        {
            gestureInfo.GetComponent<GUIText>().text = string.Empty;
        }
    }

    /// <summary>
    /// Invoked when a gesture is in progress.
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <param name="userIndex">User index</param>
    /// <param name="gesture">Gesture type</param>
    /// <param name="progress">Gesture progress [0..1]</param>
    /// <param name="joint">Joint type</param>
    /// <param name="screenPos">Normalized viewport position</param>
    public void GestureInProgress(long userId, int userIndex, KinectGestures.Gestures gesture,
                                  float progress, KinectInterop.JointType joint, Vector3 screenPos)
    {
        // the gestures are allowed for the primary user only
        KinectManager manager = KinectManager.Instance;
        if (!manager || (userId != manager.GetPrimaryUserID()))
            return;

        if ((gesture == KinectGestures.Gestures.ZoomOut || gesture == KinectGestures.Gestures.ZoomIn) && progress > 0.5f)
        {
            if (gestureInfo != null)
            {
                string sGestureText = string.Format("{0} - {1:F0}%", gesture, screenPos.z * 100f);
                gestureInfo.GetComponent<GUIText>().text = sGestureText;

                progressDisplayed = true;
                progressGestureTime = Time.realtimeSinceStartup;
            }
        }
        else if ((gesture == KinectGestures.Gestures.Wheel || gesture == KinectGestures.Gestures.LeanLeft ||
                 gesture == KinectGestures.Gestures.LeanRight || gesture == KinectGestures.Gestures.Dive) && progress > 0.5f)
        {
            if (gestureInfo != null)
            {
                string sGestureText = string.Format("{0} - {1:F0} degrees", gesture, screenPos.z);
                gestureInfo.GetComponent<GUIText>().text = sGestureText;
                print("gesture text " + sGestureText);
                print("gesture " + gesture);
                print("z: " + screenPos.z);

                progressDisplayed = true;
                progressGestureTime = Time.realtimeSinceStartup;

                if (gesture == KinectGestures.Gestures.LeanRight)
                {
                    tiltRight = true;
                    tiltLeft = false;
                    degreesRight = screenPos.z;
                }
                if (gesture == KinectGestures.Gestures.LeanLeft)
                {
                    tiltRight = false;
                    tiltLeft = true;
                    degreesLeft = screenPos.z;
                }
                if (gesture == KinectGestures.Gestures.Glide)
                {
                    glide = true;
                }
                if (gesture == KinectGestures.Gestures.Flap)
                {
                    flap = true;
                }
              /*  if (gesture == KinectGestures.Gestures.UpFlap)
                {
                    upFlap = true;
                }*/
                if (gesture == KinectGestures.Gestures.Dive)
                {
                    dive = true;
                    diveAngle = screenPos.z;
                    print("dive true");
                }
            }
        }
        else if (gesture == KinectGestures.Gestures.Run && progress > 0.5f)
        {
            if (gestureInfo != null)
            {
                string sGestureText = string.Format("{0} - progress: {1:F0}%", gesture, progress * 100);
                gestureInfo.GetComponent<GUIText>().text = sGestureText;

                progressDisplayed = true;
                progressGestureTime = Time.realtimeSinceStartup;
            }
        }
    }

    /// <summary>
    /// Invoked if a gesture is completed.
    /// </summary>
    /// <returns>true</returns>
    /// <c>false</c>
    /// <param name="userId">User ID</param>
    /// <param name="userIndex">User index</param>
    /// <param name="gesture">Gesture type</param>
    /// <param name="joint">Joint type</param>
    /// <param name="screenPos">Normalized viewport position</param>
    public bool GestureCompleted(long userId, int userIndex, KinectGestures.Gestures gesture,
                                  KinectInterop.JointType joint, Vector3 screenPos)
    {
        // the gestures are allowed for the primary user only
        KinectManager manager = KinectManager.Instance;
        if (!manager || (userId != manager.GetPrimaryUserID()))
            return false;

        if (gestureInfo != null)
        {
            string sGestureText = gesture + " detected";
            gestureInfo.GetComponent<GUIText>().text = sGestureText;
        }

        if (gesture == KinectGestures.Gestures.LeanLeft)
            tiltLeft = true;
        else if (gesture == KinectGestures.Gestures.LeanRight)
            tiltRight = true;
        else if (gesture == KinectGestures.Gestures.Glide)
        {
            glide = true;
            gestureDebug.text = "glide detected";
        }
        else if (gesture == KinectGestures.Gestures.Flap)
        {
            flap = true;
            print("REGULARflap force is " + screenPos.x + " " + screenPos.y + " " + screenPos.z);
            flapForce = screenPos; // lift + thrust force, as calculated in KinectGestures
            gestureDebug.text = "Regular Flap detected " + screenPos;
        }

        /*  else if (gesture == KinectGestures.Gestures.UpFlap)
          {
              upFlap = true;
              print("UPSTROKEflap force is " + screenPos);
              flapForce = screenPos;
              gestureInfo.GetComponent<GUIText>().text = "UPstroke flap " + screenPos;
          }*/
        else if (gesture == KinectGestures.Gestures.Dive)
        {
            dive = true;
        }
        else if (gesture == KinectGestures.Gestures.SwipeLeft || gesture == KinectGestures.Gestures.SwipeRight)
        {
            swipe = true;
        }

        return true;
    }
    
    /// <summary>
    /// Invoked if a gesture is cancelled.
    /// </summary>
    /// <returns>true</returns>
    /// <c>false</c>
    /// <param name="userId">User ID</param>
    /// <param name="userIndex">User index</param>
    /// <param name="gesture">Gesture type</param>
    /// <param name="joint">Joint type</param>
    public bool GestureCancelled(long userId, int userIndex, KinectGestures.Gestures gesture,
                                  KinectInterop.JointType joint)
    {
        // the gestures are allowed for the primary user only
        KinectManager manager = KinectManager.Instance;
        if (!manager || (userId != manager.GetPrimaryUserID()))
            return false;

        if (progressDisplayed)
        {
            progressDisplayed = false;

            if (gestureInfo != null)
            {
                gestureInfo.GetComponent<GUIText>().text = String.Empty;
            }
        }

        return true;
    }


    void Awake()
    {
        instance = this;
    }

    void Update()
    {
        if (progressDisplayed && ((Time.realtimeSinceStartup - progressGestureTime) > 2f))
        {
            progressDisplayed = false;
            gestureInfo.GetComponent<GUIText>().text = String.Empty;

            Debug.Log("Forced progress to end.");
        }
    }

}
