﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SimpleMoveScript : MonoBehaviour
{

    private FlyGestureListener gestureListener;
    private float localRotateX;
    public Rigidbody rb;
    private float maxVelocity;
    public Text helpText;
    public Canvas dialogBox; // for collision
    public AvatarController avatarController;
    bool glideLag;
    bool flapLag;
    float flapPower;
    bool turnLag;
    float turnAngle;
    public AudioClip flapSound;
    public AudioClip stepSound;
    private float lowPitchRange = .75F;
    private float highPitchRange = 1.5F;
    private AudioSource source;

    void OnCollisionEnter(Collision collision)
    {
        print("COLLISION!!! " + collision);
        print("tag is " + collision.gameObject.tag);
        if (collision.gameObject.tag == "B" || collision.gameObject.tag == "C" || collision.gameObject.tag == "A")
        {
            rb.velocity = Vector3.zero;
            // transform.rotation = new Quaternion();
            print("doForward. Rigid body vel z is now " + rb.velocity.z);
            dialogBox.gameObject.SetActive(true);
            Invoke("hideDialog", 3);
            helpText.GetComponent<Text>().text = "Raven: Uh oh--the wind is too strong here.\n" +
                "We should turn back.";
        }
    }

    void hideDialog()
    {
        dialogBox.gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start()
    {
        // get the gestures listener
        gestureListener = FlyGestureListener.Instance;
        source = GetComponent<AudioSource>();
        localRotateX = 0;
        rb = GetComponent<Rigidbody>();
        maxVelocity = 100;
        if (helpText != null)
        {
            helpText.GetComponent<Text>().text = "Keep your arms up to go forward,\n" +
                "flap and lean forward to go up and down,\n" +
                "lean left and right to turn.";
        }
    }

    // TODO: play flapping animation (or use kinect joints)
    void doFlap(Vector3 force)
    {
        rb.AddRelativeForce(force);
        // avatarController.animateFeetDrag(force); // TODO TEST THIS!!!!
        // rb.velocity = new Vector3(0, velocity, 0);
    }

    // TODO: use gesture angle for turn speed and rotation amount. Rotate animation smooth.
    void doTurnLeft(float angle)
    {
        print("rotate angle " + angle);
        //transform.RotateAround(transform.position, transform.up, Time.deltaTime * -angle);
        rb.AddTorque(0, -50 * Time.deltaTime * System.Math.Abs(angle), 0);//transform.up * -100 * Time.deltaTime);
                                                                          //  rb.AddTorque(transform.forward * angle * Time.deltaTime);
                                                                          //float currZ = transform.localEulerAngles.z;
                                                                          /* if (currZ < 30 && currZ > 0)
                                                                           {
                                                                               transform.localEulerAngles = new Vector3(
                                                                                                               0,
                                                                                                               transform.localEulerAngles.y,
                                                                                                               currZ + Time.deltaTime * 20.0f);
                                                                           }*/
    }

    void doTurnRight(float angle)
    {
        print("rotate " + angle);
        //   rb.AddTorque(transform.up * 100 * Time.deltaTime);
        rb.AddTorque(0, 50 * Time.deltaTime * System.Math.Abs(angle), 0);
        //   rb.AddTorque(transform.forward * -angle * Time.deltaTime);
        //  float currZ = transform.localEulerAngles.z;
        /*  if ((currZ > -30 && currZ < 0) || (currZ > 330 && currZ > 0))
          {
              print("currZ " + currZ);
              transform.localEulerAngles = new Vector3(
                                              0,
                                              transform.localEulerAngles.y,
                                             currZ + Time.deltaTime * -10.0f);
          }*/
    }

    void doForward()
    {
        print("do forward..");
        rb.AddForce(transform.forward * 100);
        //   rb.AddForce(5, 0, 0);
        /*transform.position += transform.forward * 600.0f * Time.deltaTime;
        Vector3 vel = rb.velocity;
    
        rb.velocity = vel;
       // rb.velocity = rb.velocity + (transform.forward * 6);
        print("doForward. Rigid body vel z is now " + rb.velocity.z);*/
    }

    /* void stopForward()
     {
         rb.velocity = new Vector3(0, 0, 0); // TODO: just change z and increase fall velocity
     }*/

    void doDive(float velocity)
    {
        rb.AddForce(0, 20 * velocity, 0);
        doForward(); // also move forward for some added speed
    }

    // TODO: make this smooth and/or use kinect for angle instead
    void undoLeanAnimation()
    {
        /* float currZ = transform.localEulerAngles.z;
         if ((currZ > 358 && currZ < 361) || (currZ > -1 && currZ < 1))
         {
             return;
         }

         if ((currZ > 1 && currZ < 180))
         {
             rb.AddRelativeTorque(0, 0, -30);
         }
         // leaning left:
         else if (currZ < -1 || (currZ < 361 && currZ > 180))
         {
             rb.AddRelativeTorque(0, 0, 30);
         }*/

        /*  print("D not pressed. A not pressed");
          float currZ = transform.localEulerAngles.z;
        // print(currZ);
          // Close to 0:
         if ((currZ > 358 && currZ < 361) || (currZ > -1 && currZ < 1))
          {
              return;
          }

         // leaning right":
          if ((currZ > 1 && currZ < 180))
          {
              transform.localEulerAngles = new Vector3(
                                              0,
                                              transform.localEulerAngles.y,
                                             currZ + Time.deltaTime * -30.0f);
          }
          // leaning left:
          else if (currZ < -1 || (currZ < 361 && currZ > 180))
          {
              transform.localEulerAngles = new Vector3(
                                          0,
                                          transform.localEulerAngles.y,
                                          currZ + Time.deltaTime * 30.0f);
          }*/
    }

    void limitRotation()
    {

        float rotationZ = transform.localEulerAngles.z;

        if (rotationZ > 180 && rotationZ < 315)
        {
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, 315);
        }
        else if (rotationZ > 45 && rotationZ < 180)
        {
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, 45);
        }
    }

    void oldUpdate()
    {
        if (rb.velocity.magnitude > maxVelocity)
        {
            rb.velocity = rb.velocity.normalized * maxVelocity;
        }


        if (!gestureListener)
        {
            print("listener did not exist.");

            return;
        }

        // Keyboard controls:

        if (Input.GetKey(KeyCode.A))
        {
            doTurnLeft(50);

        }
        if (Input.GetKey(KeyCode.D))
        {
            doTurnRight(50);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            doDive(-30);
        }

        if (!Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.A))
        {
            undoLeanAnimation();
        }

        if (Input.GetKey(KeyCode.W))
        {
            doForward();
        }

        // Todo use physics + forces
        if (Input.GetKey(KeyCode.E))
        {
            doFlap(new Vector3(0, 500, 0));
        }

        // Kinect controls:
        if (gestureListener.IsFlap() || gestureListener.IsUpFlap())
        {
            Vector3 force = gestureListener.getFlapForce();
            print("flap force is " + force.y);
            if (force.y < 0)
            {
                print("TODO - FLAP FORCE IS NEGATIVE. Returning for now");
                return;

            }
            else {
                doFlap(force);
            }
        }
        /*if (gestureListener.IsUpFlap())
        {
            Vector3 force = gestureListener.getFlapForce();
            print("UPflap force is " + force.y);
            doFlap(force);
        }*/


        // TODO: if not glide, fall faster
        if (gestureListener.IsGlide())
        {
            doForward();
        }
        else
        {
            //   stopForward();
        }

        print("UPDATE. GESTURE LISTENER IS " + gestureListener);
        // TODO: still move foward
        // TODO: roll rotation
        if (gestureListener.IsTiltRight())
        {
            print("TILT RIGHT!");
            //            transform.Rotate(0.0f, gestureListener.getDegreesRight() * Time.deltaTime, 0.0f);
            doTurnRight(gestureListener.getDegreesRight());

        }

        if (gestureListener.IsTiltLeft())
        {
            print("TILT LEFT!");
            //transform.Rotate(0.0f, -gestureListener.getDegreesRight() * Time.deltaTime, 0.0f);
            doTurnLeft(gestureListener.getDegreesLeft());
        }
        if (gestureListener.IsDive())
        {
            doDive(-gestureListener.getDiveAngle());
        }
        limitRotation();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        print("FIXED UPDATE");

        float defAcc = 100;
        float defAngle = 25;
        float maxAngle = 180;
        float gravity = -9.81f;
        //  float vTerminalVertical = 150; // TODO - depends on arm area
        float topSpeed = 100; // TODO - depends on arm area also maybe diff up/down vs. leftright vs. forward/back


        // KEY CONTROLS

        // Turn
        if (Input.GetKey(KeyCode.A))
        {
            rb.transform.Rotate(0, -defAngle * Time.deltaTime, 0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            rb.transform.Rotate(0, defAngle * Time.deltaTime, 0);
        }

        // Up/Down
        if (Input.GetKey(KeyCode.Q))
        {
            float drag = Mathf.Abs((gravity + defAcc) / topSpeed);
            rb.velocity += (Vector3.up * (gravity - defAcc) - (drag * rb.velocity)) * Time.fixedDeltaTime;
        }
        if (Input.GetKey(KeyCode.E))
        {
            source.pitch = Random.Range(lowPitchRange, highPitchRange);
            source.PlayOneShot(flapSound);
            float drag = Mathf.Abs((gravity - defAcc) / topSpeed);
            rb.velocity += (Vector3.up * (gravity + defAcc) - (drag * rb.velocity)) * Time.fixedDeltaTime;
        }

        // Forward/Back
        if (Input.GetKey(KeyCode.W))
        {
            //  rb.velocity += (transform.forward * defSpeed * Time.deltaTime);
            float drag = defAcc / topSpeed;
            rb.velocity += (rb.transform.forward * (defAcc) - (drag * rb.velocity)) * Time.fixedDeltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            float drag = -defAcc / topSpeed;
            rb.velocity += (transform.forward * (-defAcc) - (drag * rb.velocity)) * Time.fixedDeltaTime;
        }


        // GESTURE CONTROLS

        bool usedGravity = false;

        // Left/Right
        if (gestureListener.IsTiltLeft())
        {

            float angle = gestureListener.getDegreesLeft();
            angle = (Mathf.Abs(angle) / 90) * maxAngle;
            rb.transform.Rotate(0, -1.5f*angle * Time.deltaTime, 0);
        }
        if (gestureListener.IsTiltRight())
        {
            float angle = gestureListener.getDegreesRight();
            angle = (Mathf.Abs(angle) / 90) * maxAngle;
            rb.transform.Rotate(0, 1.5f*angle *Time.deltaTime, 0);
        }

        // Up/Down
        if (gestureListener.IsFlap())
        {
            // TODO: use direction of force
            float acceleration = gestureListener.getFlapForce().magnitude;
            Vector3 dirAcc = Vector3.Normalize(gestureListener.getFlapForce());
            acceleration = Mathf.Min(200, (acceleration / 5000f) * 150f); // from observation - todo
            print("FLAP MAG " + acceleration);
            float drag = Mathf.Abs((gravity + acceleration) / topSpeed);

            // TODO - transform force direction to local space
            rb.velocity += (rb.transform.up * (10*acceleration + gravity) - (drag * rb.velocity)) * Time.fixedDeltaTime;
            usedGravity = true;
            source.pitch = Random.Range(lowPitchRange, highPitchRange);
            source.PlayOneShot(flapSound);
        }
        if (gestureListener.IsUpFlap())
        {
            // TODO.
        }
        if (gestureListener.IsDive())
        {
            print("DIVE");
            float acceleration = gestureListener.getDiveAngle();
            acceleration = -(Mathf.Abs(acceleration) / 90) * maxVelocity;
            float drag = Mathf.Abs((gravity + acceleration) / topSpeed);
            print("dive accel " + 2*acceleration + " drag " + drag);
            //if (rb.velocity.y > -maxVelocity) // TODO
           // {
                rb.velocity += (rb.transform.up * (gravity + 2*acceleration) - (drag * rb.velocity)) * Time.fixedDeltaTime;


                // forward too:
                drag = Mathf.Abs(defAcc) / topSpeed;
                rb.velocity += (rb.transform.forward * (2*defAcc) - (drag * rb.velocity)) * Time.fixedDeltaTime;

                // TODO: wingspan here too
//            }

            usedGravity = true;
        }

        
        float sa = gestureListener.getParallelSurfaceArea();
        print("SA is  " + sa);
        // FORWARD/BACK
        if (gestureListener.IsGlideStrict() || sa > 15)
        {
            {
               // glideLag = true;
                print("GLIDE!");
                float drag = Mathf.Abs(defAcc) / topSpeed;
                rb.velocity += (rb.transform.forward * (15*defAcc) - (drag * rb.velocity)) * Time.fixedDeltaTime;
                //  Invoke("glideLagFalse", .1f);

                // gravity
               /* float maxSA = 40; // based on observation
                float gravityAdjust = gravity * (1- (sa / maxSA));
                rb.velocity += (rb.transform.up * (gravityAdjust) - (drag * rb.velocity)) * Time.fixedDeltaTime;

                print("SURF IS " + sa);
                usedGravity = true;*/
            }
        }

        print("check used gravity");
        if(!usedGravity)
        {
           
            print("do gravity");
            // gravity
            float drag = Mathf.Abs(gravity) / topSpeed;
            float maxSA = 35; // based on observation
            // adjust gravity to 0 at minimum depending on surface area
            float gravityAdjust = gravity * (Mathf.Min(1, 1 - (sa / maxSA)));
            print("gravity adjust pre " + gravityAdjust);
            gravityAdjust = Mathf.Min(0, gravityAdjust);
            print("gravity adjust is " + gravityAdjust);
            if (rb.velocity.y > -maxVelocity)
            {
                rb.velocity += (rb.transform.up * (gravityAdjust) - (drag * rb.velocity)) * Time.fixedDeltaTime;
            }
            if (sa > 5 && rb.velocity.y < 0)
            {
                rb.velocity += (rb.transform.forward * (2 * defAcc) - (drag * rb.velocity)) * Time.fixedDeltaTime;
            }
        }
    }

    void glideLagFalse()
    {
        print("glide lag false");
        glideLag = false;
    }

    void turnLagFalse()
    {
        turnLag = false;
    }

    void flapLagFalse()
    {
        flapLag = false;
    }
}