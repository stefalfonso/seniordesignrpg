﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;

public class Aves : MonoBehaviour
{
    public int currentIsland = 0; // the island the Aves is
    private int currentLevel = 0; // level island has unlocked (A, B, C, D)

    public void setPosition()
    {
        print("setting position currentIsland " + currentIsland);
        if (currentIsland == 0)
        {
            Vector3 ipos = IslandScript.dockPos["Tarst"]; //IslandScript.islandPos["Tarst"];
            ipos.y += 10f;
            transform.position = ipos;
            transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
            print("pos is " + ipos + "\n rot is " + transform.rotation);
        }
        if (currentIsland == 1)
        {
            Vector3 ipos = IslandScript.dockPos["Thwemoon"];//IslandScript.islandPos["Thwemoon"];
            ipos.y += 100f;
            transform.position = ipos;
        }

        if (currentIsland == 2)
        {
            Vector3 ipos = IslandScript.islandPos["Spigun"];
            ipos.y += 100f;
            transform.position = ipos;
        }

        if (currentIsland == 3)
        {
            Vector3 ipos = IslandScript.islandPos["Nalif"];
            ipos.y += 100f;
            transform.position = ipos;
        }
else
        {
            Vector3 ipos = IslandScript.dockPos["Tarst"]; //IslandScript.islandPos["Tarst"];
            ipos.y += 10f;
            transform.position = ipos;
            transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
            print("pos is " + ipos + "\n rot is " + transform.rotation);
        }
        transform.rotation = new Quaternion();
    }

    // Use this for initialization
    void Start()
    {
        currentLevel = Int32.Parse(FindObjectOfType<AvesInfo>().getValue("accessLevel"));
        setPosition();
        setupBarriers();
        // DontDestroyOnLoad(this);
    }

    void setupBarriers()
    {
        // get barriers:
        GameObject[] Abarriers = GameObject.FindGameObjectsWithTag("A");
        GameObject[] Bbarriers = GameObject.FindGameObjectsWithTag("B");
        GameObject[] Cbarriers = GameObject.FindGameObjectsWithTag("C");

        // disable all barriers:
        print("curr level " + currentLevel);
        for (int i = 0; i < Abarriers.Length; i++)
        {
            Abarriers[i].GetComponent<Collider>().enabled = false;
            print("true aberrier");
        }
        for (int i = 0; i < Bbarriers.Length; i++)
        {
            Bbarriers[i].GetComponent<Collider>().enabled = false;
        }

        for (int i = 0; i < Cbarriers.Length; i++)
        {
            Cbarriers[i].GetComponent<Collider>().enabled = false;
        }

        if (currentLevel == 0)
        {
            for (int i = 0; i < Abarriers.Length; i++)
            {
                Abarriers[i].GetComponent<Collider>().enabled = true;
                print("true aberrier");
            }
        }
        else if (currentLevel == 1)
        {
            for (int i = 0; i < Bbarriers.Length; i++)
            {
                Bbarriers[i].GetComponent<Collider>().enabled = true;
            }
        }
        else if (currentLevel == 2)
        {
            for (int i = 0; i < Cbarriers.Length; i++)
            {
                Cbarriers[i].GetComponent<Collider>().enabled = true;
            }
        }
    }


    // Update is called once per frame
    void Update()
    {

    }
}