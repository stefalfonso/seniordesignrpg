﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;

public class IslandScript : MonoBehaviour {

    int islandIdx;
    public PositionMenu yesNoMenu;
    private GameObject dock;
    public VRInteractiveItem yes;
    public VRInteractiveItem no;
    private FlyGestureListener gestureListener = FlyGestureListener.Instance;
    private string collidedWith = null; // if collided with something and not selected from menu this won't be null


    public static readonly Dictionary<string, Vector3> islandPos = new Dictionary<string, Vector3>
    {

        {"Tarst", new Vector3(0,0,0) },
        {"Thwemoon", new Vector3(0,0,0) },
        {"Spigun", new Vector3(0,0,0) },
        {"Nalif", new Vector3(0,0,0) }
};

    public static readonly Dictionary<string, Vector3> dockPos = new Dictionary<string, Vector3>
    {

        {"Tarst", new Vector3(0,0,0) },
        {"Thwemoon", new Vector3(0,0,0) },
        {"Spigun", new Vector3(0,0,0) },
        {"Nalif", new Vector3(0,0,0) }
};

    float maxUpAndDown = 1;             // amount of meters going up and down
    float speed = 100;            // up and down speed
    float angle = 0;            // angle to determin the height by using the sinus
    float toDegrees = Mathf.PI/180;    // radians to degrees


    public int getIslandIndex()
    {
        return islandIdx;
    }

    public void setIslandIndex(int islandIdx)
    {
        this.islandIdx = islandIdx;
    }
    
    GameObject getChildWithTag(string tag)
    {

        foreach (Transform child in transform)
            {
            if (child.gameObject.tag == tag)
            {
                return child.gameObject;
            }
        }
        return null;
    }
    void initTarst()
    {
        islandIdx = 0;
        islandPos["Tarst"] = transform.position;
        dockPos["Tarst"] = dock.transform.position;
        //print("islandpos tarst is " + islandPos["Tarst"]);
    }

    void initThwemoon()
    {
        islandIdx = 1;
        islandPos["Thwemoon"] = transform.position;
        dockPos["Thwemoon"] = dock.transform.position;
        print("init thwemoon pos to be " + transform.position);
    }

    void initSpigun()
    {
        islandIdx = 2;
        islandPos["Spigun"] = transform.position;
        dockPos["Spigun"] = dock.transform.position;
    }

    void initNalif()
    {
        islandIdx = 3;
        islandPos["Nalif"] = transform.position;
        dockPos["Nalif"] = dock.transform.position;
    }

    // Use this for initialization
    void Start () {

        dock = getChildWithTag("dock");
       
   
        if ((this.gameObject.name == "Tarst"))
        {
            initTarst();
        }
        if ((this.gameObject.name == "Thwemoon"))
        {
                initThwemoon();
        }
        if ((this.gameObject.name == "Spigun"))
        {
                initSpigun();
        }
        if ((this.gameObject.name == "Nalif"))
        {
                initNalif();
        
        }
    }

    void OnEnabled()
    {
        no.OnOver += SelectNo;
        yes.OnOver += SelectYes;
    }

    void OnDisabled()
    {
        no.OnOver -= SelectNo;
        yes.OnOver -= SelectYes;
    }

    void SelectYes()
    {
        if (gestureListener.isSwipe() || Input.GetKey(KeyCode.R))
        {
            // select
            SceneManager.LoadScene(collidedWith + "-dialog"); // TODO - find out actual next scene  
            print("SELECT YES!");

            // the following is probably not necessary:
            collidedWith = null;
           // yesNoMenu.Hide();
        }
    }

    void SelectNo()
    {
        if (gestureListener.isSwipe() || Input.GetKey(KeyCode.R))
        {
            // select - hide menu + ignore collision
            print("SELECT NO!");
            collidedWith = null;
            yesNoMenu.Hide();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        print("COLLISION!!! " + collision);
        if ((this.gameObject.name == "Tarst"))
        {
            print("Tarst entered");
            yesNoMenu.Display();
            collidedWith = "Tarst";
           // SceneManager.LoadScene("tarst-dialog");
        }
        if ((this.gameObject.name == "Thwemoon"))
        {
            print("Thwemoon entered");
            yesNoMenu.Display();
            collidedWith = "Thwemoon";
          //  SceneManager.LoadScene("thwemoon-dialog");
        }
        if ((this.gameObject.name == "Spigun"))
        {
            print("Spigun entered");
            yesNoMenu.Display();
            collidedWith = "Spigun";
           // SceneManager.LoadScene("spigun-dialog");
        }
        if ((this.gameObject.name == "Nalif"))
        {
            //  SceneManager.LoadScene("nalif-dialog");
            collidedWith = "Nalif";
            yesNoMenu.Display();
            print("Nalif entered");
        }
    }

    // Update is called once per frame
    void Update () {
        // print("island pos is " + transform.position);
        Vector3 docko = dock.transform.position;
        angle += speed * Time.deltaTime;
        if (angle > 360) angle -= 360;
        Vector3 p = transform.position;
        p.y += maxUpAndDown * Mathf.Sin(angle * toDegrees);
        transform.position = p;
        // keep dock stationary
        dock.transform.position = docko;
    }
}
