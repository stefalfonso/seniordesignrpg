﻿using UnityEngine;
using System.Collections;

public class ButtonEvents : MonoBehaviour {
    private ButtonGestureListener gestureListener;

    // Use this for initialization
    void Start () {
        // get the gestures listener
        gestureListener = ButtonGestureListener.Instance;
    }
	
	// Update is called once per frame
	void Update () {
        print("UPDATE");
        if (!gestureListener)
        {
            print("listener did not exist");
            return;
        }
            print("swipe right is" + gestureListener.IsSwipeRight());
            if (gestureListener.IsSwipeRight() || gestureListener.IsSwipeLeft())
            {
                // TODO: insert a check if oculus is on this button or move this check to oculus gaze script.
                print("CLICKED! Openning skyworld");
                Application.LoadLevel("skyworld");
        }
	
	}
}
