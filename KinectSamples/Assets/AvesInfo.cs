﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class AvesInfo : MonoBehaviour {

    public string txtFilename  = "Resources/UserInfo.txt";
    private TextAsset txtFile; // txt file where we load and store user info


    // default values
    protected readonly Dictionary<string, string> info = new Dictionary<string, string>
    {
        { "name", "Raven" },
        {"level", "1" }, // level in battle. Used for evolving and how much experience to gain.
        {"characterType", "human" }, // type of character the user is
        {"attack", "14" }, 
        { "defense", "10" },
        {"speed", "10" },
        {"MP", "10" }, // magic power
        {"HP", "100" }, // health
        {"xp", "0" }, // experience
        { "characterDescription", "Sack of flesh and bones. Strong but fragile." },
        {"accessLevel", "1" }, // levels of access 1 means you can fly to Tarst and Thwemoon, 2 means third island, 3 is all islands.
        // May unlock other things as well (like items).
        {"episodeCompleted", "0" }, // what step of this scene is the user on
        {"currentScene", "tarst-dialog" }, // what scene to load
    };

    void loadFromFile()
    {
        string[] dialogLines = txtFile.text.Split('\n');
        for(int i = 0; i < dialogLines.Length; i++)
        {
            string[] keyvalue = dialogLines[i].Split('*');
            if (info.ContainsKey(keyvalue[0]))
            {
                info[keyvalue[0]] = keyvalue[1];
            } else
            {
                info.Add(keyvalue[0], keyvalue[1]);
            }
        }
    }

    // Call this before loading a new scene or exiting so everything gets saved
    public void storeToFile()
    {
        string text = "";

        foreach (KeyValuePair<string, string> entry in info)
        {
            text += entry.Key + "*" + entry.Value + "\n";
        }

        File.WriteAllText(txtFilename, text);
    }

    // Use this for initialization
    void Start () {
        // storeToFile(); // JUST FOR TESTING.
        txtFile = Resources.Load(txtFilename) as TextAsset;
        if (txtFile)
        {
            loadFromFile();
        } else
        {
            print("houston, we have a problem. no textasset loaded");
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void setValue(string key, string value)
    {
        info[key] = value;
    }

    public void addValue(string key, string value)
    {
        info.Add(key, value);
    }

    public string getValue(string key)
    {
        return info[key];
    }
}
