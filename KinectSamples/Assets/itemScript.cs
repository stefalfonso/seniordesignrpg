﻿using UnityEngine;
using System.Collections;

public class itemScript : MonoBehaviour {

    public string itemName;
    public string description;
    public int price;
    public UnityEngine.UI.Text descriptText;

    private bool active = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
        if(active)
        {
            // rotate
            transform.Rotate(Time.deltaTime * new Vector3(0, 45, 0));
            descriptText.text = itemName + "\n" + price + " coins \n" + description;
        }
        else
        {
            print("not active item is " + itemName);
           
        }
	}

    public void setActive()
    {
        active = true;
        print("set active");
    }

    public void setInactive()
    {
        print("set inactive");
        active = false;
    }
}
