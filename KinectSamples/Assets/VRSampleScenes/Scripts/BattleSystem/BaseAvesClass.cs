﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BaseAvesClass : BaseCharacterClass
{
    public BaseAvesClass()
    {
        characterType = "Aves";
        characterDescription = "A human's lifelong, magical companion. Hardy and fast.";
        attack = 8;
        defense = 12;
        speed = 16;
        MP = 10;
        HP = 100;
    }

    new public void Start()
    {
        //stat1 = GameObject.Find("PStats").GetComponent<Text>();
    }

    new public void Update()
    {
        stat1.text = "Type: " + characterType + "\n" +
            "Attack: " + attack + "\n" +
            "Defense: " + defense + "\n" +
            "Speed: " + speed + "\n" +
            "MP: " + MP;

        if (HP > 0)
        {
            HPtext.text = "HP: " + HP + "/100";
        }
    }
}
