﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BaseCharacterClass : MonoBehaviour {
    public string characterType; // Human or Aves
    public string characterDescription; // A short description
    //stats
    public int attack; // max = 100
    public int defense;// max = 100
    public int speed; // max = 100
    public int MP; // max = 100
    public int HP; // max = 800

    public Text stat1;
    public Text HPtext;

    public TurnBasedCombatStateMachine tbsm;

    public BaseCharacterClass()
    {

    }

    // copy constructor for battling
    public void copyFrom(BaseCharacterClass original)
    {
        this.characterType = original.characterType;
        this.characterDescription = original.characterDescription;
        this.attack = original.attack;
        this.defense = original.defense;
        this.speed = original.speed;
        this.MP = original.MP;
        this.HP = original.HP;

        this.stat1 = original.stat1;
        this.HPtext = original.HPtext;
    }

    public void Start()
    {
        //stat1 = GameObject.Find("PStats").GetComponent<Text>();
    }

    public void Update()
    {
        //stat1.text = characterType + characterDescription + attack + defense + speed + MP + HP;
    }
}
