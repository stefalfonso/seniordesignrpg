﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BattleController : MonoBehaviour
{
    public BaseHumanClass playerChar;
    public BaseAvesClass playerAves;
    public BaseHumanClass enemyChar;
    public BaseAvesClass enemyAves;

    public AttackMove PCMove;
    public AttackMove PAMove;
    public AttackMove ECMove;
    public AttackMove EAMove;

    public TurnBasedCombatStateMachine stateMachine;
    public GameObject losingMessage;
    public GameObject winningMessage;

    public HideorShowMenu hideShowMenuScript;
    public GameObject battleMenu;

    public Text battleText;
    public string defaultBattleText;

    private ButtonGestureListener gestureListener;

    // Use this for initialization
    void Start()
    {
        gestureListener = ButtonGestureListener.Instance;
        defaultBattleText = "You are in the middle of a battle! \n(Open / Close menu: press space or swipe down)";
    }

    // Update is called once per frame
    void Update()
    {
        //if stateMachine says battle is in ENEMYMOVE state, calculate enemy Move
        if (stateMachine.inEnemyMove())
        {
            calculateEnemyMove();
        } else if (stateMachine.inCheckWinLose())
        {
            checkWinLose();
        } else if (stateMachine.inWinLose())
        {
            if (gestureListener.IsRaiseRightHand() || Input.GetKeyDown(KeyCode.Return))
            {
                SceneManager.LoadScene("tarst-dialog");
            }
        }
    }

    void calculateEnemyMove()
    {
        ECMove = new AttackMove();
        EAMove = new AttackMove();
        ECMove.name = "Strike";
        EAMove.name = "Dark Spell";
        ECMove.damageDealt = 40;
        EAMove.damageDealt = 40;

        if (!stateMachine.playerAvesFainted && !stateMachine.playerCharFainted)
        {
            EAMove.target = playerAves;
            ECMove.target = playerChar;
        } else if (!stateMachine.playerAvesFainted && stateMachine.playerCharFainted)
        {
            EAMove.target = playerAves;
            ECMove.target = playerAves;
        }
        else if (stateMachine.playerAvesFainted && !stateMachine.playerCharFainted)
        {
            EAMove.target = playerChar;
            ECMove.target = playerChar;
        }

        calculateEnemyDamage();
        calculatePlayerDamage();

        stateMachine.advanceTo(4);
    }

    void calculateEnemyDamage()
    {
        if (!stateMachine.playerAvesFainted && !stateMachine.playerCharFainted)
        {
            if (PCMove.name != PAMove.name)
            {
                PCMove.target.HP -= PCMove.damageDealt;
                PAMove.target.HP -= PAMove.damageDealt;

                battleText.text = "Player Char used " + PCMove.name + " on " + PCMove.target.name + "!\n" +
                "Player Aves used " + PAMove.name + " on " + PAMove.target.name + "!\n";
            }
            else {
                if (PCMove.hasTarget(0))
                {
                    PCMove.target.HP -= PCMove.damageDealt;
                    if (PAMove.hasTarget(1))
                    {
                        PAMove.target1.HP -= PAMove.damageDealt;
                        battleText.text = "Player Char used " + PCMove.name + " on " + PCMove.target.name + "!\n" +
                        "Player Aves used " + PAMove.name + " on " + PAMove.target1.name + "!\n";
                    }
                }
            }
        } else if (!stateMachine.playerAvesFainted && stateMachine.playerCharFainted)
        {
            if (PAMove.hasTarget(1))
            {
                PAMove.target1.HP -= PAMove.damageDealt;
                battleText.text = "Player Aves used " + PAMove.name + " on " + PAMove.target1.name + "!\n";
            } else
            {
                PAMove.target.HP -= PAMove.damageDealt;
                battleText.text = "Player Aves used " + PAMove.name + " on " + PAMove.target.name + "!\n";
            }
        } else if (stateMachine.playerAvesFainted && !stateMachine.playerCharFainted)
        {
            if (PCMove.hasTarget(1))
            {
                PCMove.target1.HP -= PCMove.damageDealt;
                battleText.text = "Player Char used " + PCMove.name + " on " + PCMove.target1.name + "!\n";
            }
            else
            {
                PCMove.target.HP -= PCMove.damageDealt;
                battleText.text = "Player Char used " + PCMove.name + " on " + PCMove.target.name + "!\n";
            }
        }
    }

    void calculatePlayerDamage()
    {
        if (!stateMachine.enemyAvesFainted && !stateMachine.enemyCharFainted)
        {
            EAMove.target.HP -= EAMove.damageDealt;
            ECMove.target.HP -= ECMove.damageDealt;
            battleText.text += "Enemy Char used " + ECMove.name + " on " + ECMove.target.name + "!\n" +
                "Enemy Aves used " + EAMove.name + " on " + EAMove.target.name + "!\n";
        } else if (stateMachine.enemyAvesFainted && !stateMachine.enemyCharFainted)
        {
            ECMove.target.HP -= ECMove.damageDealt;
            battleText.text += "Enemy Char used " + ECMove.name + " on " + ECMove.target.name + "!\n";
        } else if (!stateMachine.enemyAvesFainted && stateMachine.enemyCharFainted)
        {
            EAMove.target.HP -= EAMove.damageDealt;
            battleText.text += "Enemy Aves used " + EAMove.name + " on " + EAMove.target.name + "!\n";
        } 
    }

    void checkWinLose()
    {
        if (enemyAves.HP <= 0 && !stateMachine.enemyAvesFainted)
        {
            enemyAves.HPtext.text = "HP: fainted";
            stateMachine.enemyAvesFainted = true;
            GameObject.Find("Enemy Aves").SetActive(false);
        }

        if (enemyChar.HP <= 0 && !stateMachine.enemyCharFainted) {
            enemyChar.HPtext.text = "HP: fainted";
            stateMachine.enemyCharFainted = true;
            GameObject.Find("Enemy Character").SetActive(false);
        }

        if (enemyAves.HP <= 0 && enemyChar.HP <= 0)
        {
            // player wins 
            winningMessage.SetActive(true);
            stateMachine.advanceTo(5);
        }

        if (playerAves.HP <= 0 && !stateMachine.playerAvesFainted)
        {
            playerAves.HPtext.text = "HP: fainted";
            stateMachine.playerAvesFainted = true;
            GameObject.Find("Player Aves").SetActive(false);
        }

        if (playerChar.HP <= 0 && !stateMachine.playerCharFainted)
        {
            playerChar.HPtext.text = "HP: fainted";
            stateMachine.playerCharFainted = true;
            GameObject.Find("Player Character").SetActive(false);
        }

        if (playerAves.HP <= 0 && playerChar.HP <=0)
        {
            // player loses
            losingMessage.SetActive(true);
            stateMachine.advanceTo(6);
        }

        if (!stateMachine.inWinLose())
        {
            stateMachine.advanceTo(0);
   
            battleMenu.SetActive(true);
            hideShowMenuScript.menuToShowHide = "Menu";
            hideShowMenuScript.menuItems = battleMenu;
            hideShowMenuScript.Start();
            hideShowMenuScript.Show();
        }
    }
}
