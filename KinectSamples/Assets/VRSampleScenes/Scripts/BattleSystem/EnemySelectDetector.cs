﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class EnemySelectDetector : MonoBehaviour {
    private ButtonGestureListener gestureListener;
    public Animator animator;
    public GameObject PCselectedEnemy; // default is Enemy character
    public GameObject PAselectedEnemy; // default is Enemy aves
    public GameObject selector;
    public GameObject attackConfirmationText;
    public GameObject attackMenu;
    public AttackMenuButton attackMenuBtn;
    public TurnBasedCombatStateMachine stateMachine;
    public HideorShowMenu hideShowMenuScript;
    public GameObject battleMenu;
    public BattleController battleController;
    private bool submittingInProcess;

    // Use this for initialization
    public void Start() {
        // get the gestures listener
        gestureListener = ButtonGestureListener.Instance;
        animator = GetComponent<Animator>();
        submittingInProcess = false;
    }

    // Update is called once per frame
    void Update() {
        //print("PC attack target is " + battleController.PCMove.target.characterType);
        //print("PA attack target is " + battleController.PAMove.target.characterType);
        //Kinect gesture detection
        if (!gestureListener)
        {
            print("listener did not exist");
            //return;
        }
        if (!stateMachine.enemyCharFainted && !stateMachine.enemyAvesFainted)
        {
            if (gestureListener.IsSwipeRight() || Input.GetKeyDown(KeyCode.RightArrow))
            {
                //print("swiping right");
                animator.SetTrigger("SwipeRight"); //play swipe right animation
                if (stateMachine.inPlayerMove())
                {
                    PCselectedEnemy = GameObject.Find("Enemy Character"); //set target to Enemy Char
                    print("selected enemy is " + PCselectedEnemy.ToString());
                }
                else
                {
                    PAselectedEnemy = GameObject.Find("Enemy Character");
                    print("selected enemy is " + PAselectedEnemy.ToString());
                }
            }

            if (gestureListener.IsSwipeLeft() || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                //print("swiping left");
                animator.SetTrigger("SwipeLeft"); //play swipe left animation
                if (stateMachine.inPlayerMove())
                {
                    PCselectedEnemy = GameObject.Find("Enemy Aves"); //set target to Enemy Char
                    print("selected enemy is " + PCselectedEnemy.ToString());
                }
                else
                {
                    PAselectedEnemy = GameObject.Find("Enemy Aves");
                    print("selected enemy is " + PAselectedEnemy.ToString());
                }
            }

            if (gestureListener.IsRaiseRightHand() || Input.GetKeyDown(KeyCode.Return))
            {
                if (stateMachine.inPlayerMove())
                {
                    if (selector.transform.position.x > 3.8)
                    {
                        PCselectedEnemy = GameObject.Find("Enemy Character");
                    }
                    else
                    {
                        PCselectedEnemy = GameObject.Find("Enemy Aves");
                    }
                    submitPCEnemy();
                }
                else
                {
                    if (selector.transform.position.x > 3.8)
                    {
                        PAselectedEnemy = GameObject.Find("Enemy Character");
                    }
                    else
                    {
                        PAselectedEnemy = GameObject.Find("Enemy Aves");
                    }
                    submitPAEnemy();
                }
            }

            if (gestureListener.IsRaiseLeftHand() || Input.GetKeyDown(KeyCode.Backspace))
            {
                //cancel 
                if (selector.activeSelf == true)
                {
                    returnToAtkMenu();
                }
            }

            if (submittingInProcess)
            {
                if (selector.transform.position.x > 3.8)
                {
                    if (stateMachine.inPlayerMove())
                    {
                        submittingInProcess = false;
                        PCselectedEnemy = GameObject.Find("Enemy Character");
                        stateMachine.advanceTo(2);
                        returnToAtkMenu();
                    }
                    else
                    {
                        submittingInProcess = false;
                        PAselectedEnemy = GameObject.Find("Enemy Character");
                        stateMachine.advanceTo(3);
                        selector.SetActive(false);
                        attackConfirmationText.SetActive(false);
                        //play Enemy Animation 
                    }
                }
            }
        } else
        {
            if (stateMachine.enemyCharFainted)
            {
                animator.SetTrigger("SwipeLeft");
                if (gestureListener.IsRaiseRightHand() || Input.GetKeyDown(KeyCode.Return))
                {
                    if (stateMachine.inPlayerMove())
                    {
                        PCselectedEnemy = GameObject.Find("Enemy Aves");
                        submitPCEnemy();
                    }
                    else
                    {
                        PAselectedEnemy = GameObject.Find("Enemy Aves");
                        submitPAEnemy();
                    }
                }

                if (submittingInProcess)
                {
                    if (selector.transform.position.x > 3.8)
                    {
                        if (stateMachine.inPlayerMove())
                        {
                            submittingInProcess = false;
                            PCselectedEnemy = GameObject.Find("Enemy Character");
                            stateMachine.advanceTo(2);
                            returnToAtkMenu();
                        }
                        else
                        {
                            submittingInProcess = false;
                            PAselectedEnemy = GameObject.Find("Enemy Character");
                            stateMachine.advanceTo(3);
                            selector.SetActive(false);
                            attackConfirmationText.SetActive(false);
                            //play Enemy Animation 
                        }
                    }
                }
            } else if (stateMachine.enemyAvesFainted)
            {
                if (gestureListener.IsRaiseRightHand() || Input.GetKeyDown(KeyCode.Return))
                {
                    if (stateMachine.inPlayerMove())
                    {
                        PCselectedEnemy = GameObject.Find("Enemy Character");
                        submitPCEnemy();
                    }
                    else
                    {
                        PAselectedEnemy = GameObject.Find("Enemy Character");
                        submitPAEnemy();
                    }
                }

                if (submittingInProcess)
                {
                    if (selector.transform.position.x > 3.8)
                    {
                        if (stateMachine.inPlayerMove())
                        {
                            submittingInProcess = false;
                            PCselectedEnemy = GameObject.Find("Enemy Character");
                            stateMachine.advanceTo(2);
                            returnToAtkMenu();
                        }
                        else
                        {
                            submittingInProcess = false;
                            PAselectedEnemy = GameObject.Find("Enemy Character");
                            stateMachine.advanceTo(3);
                            selector.SetActive(false);
                            attackConfirmationText.SetActive(false);
                            //play Enemy Animation 
                        }
                    }
                }
            } 
        }
    }

    void submitPCEnemy()
    {
        //submit selected enemy 
        BaseCharacterClass PCEnemyCopy = PCselectedEnemy.GetComponent<BaseCharacterClass>();
        attackMenuBtn.attack.target = PCEnemyCopy;
        battleController.PCMove = attackMenuBtn.attack;

        animator.SetTrigger("SwipeRight");
        submittingInProcess = true;
    }

    void submitPAEnemy()
    {
        BaseCharacterClass PAEnemyCopy = PAselectedEnemy.GetComponent<BaseCharacterClass>();
        attackMenuBtn.attack.target1 = PAEnemyCopy;
        battleController.PAMove = attackMenuBtn.attack;


        animator.SetTrigger("SwipeRight");
        submittingInProcess = true;
    }

    void returnToAtkMenu()
    {
        // hide selector and go back to attack menu
        attackConfirmationText.SetActive(false);
        attackMenu.SetActive(true);
        selector.SetActive(false);
    }
}
