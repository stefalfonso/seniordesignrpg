using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;



// This script is for loading scenes from the main menu.
// Each 'button' will be a rendering showing the scene
// that will be loaded and use the SelectionRadial.
public class AttackMenuButton : MonoBehaviour
{
    public event Action<AttackMenuButton> OnButtonSelected;                   // This event is triggered when the selection of the button has finished.

    public delegate void Selected();
    public static event Selected OnSelect;
    public HideorShowObject hideShowObjectScript;
    public GameObject shownObject;
    public string shownObjectName;
    public GameObject hiddenMenu;
    public GameObject enemySelectorPyramid;
    public EnemySelectDetector enemySelector;
    public AttackMove attack;
    public GameObject attackMenu;
    public TurnBasedCombatStateMachine stateMachine;

    [SerializeField] private VRCameraFade m_CameraFade;                 // This fades the scene out when a new scene is about to be loaded.
    [SerializeField] private SelectionRadial m_SelectionRadial;         // This controls when the selection is complete.
    [SerializeField] private VRInteractiveItem m_InteractiveItem;       // The interactive item for where the user should click to load the level.

    private bool m_GazeOver;                                            // Whether the user is looking at the VRInteractiveItem currently.

    public bool isActivated = false; 

    private void OnEnable ()
    {
        //disable default menu
        hiddenMenu.SetActive(false);

        m_InteractiveItem.OnOver += HandleOver;
        m_InteractiveItem.OnOut += HandleOut;
        m_SelectionRadial.OnSelectionComplete += HandleSelectionComplete;
    }


    private void OnDisable ()
    {
        m_InteractiveItem.OnOver -= HandleOver;
        m_InteractiveItem.OnOut -= HandleOut;
        m_SelectionRadial.OnSelectionComplete -= HandleSelectionComplete;
    }
        

    private void HandleOver()
    {
        // When the user looks at the rendering of the scene, show the radial.
        m_SelectionRadial.Show();

        m_GazeOver = true;
    }


    private void HandleOut()
    {
        // When the user looks away from the rendering of the scene, hide the radial.
        m_SelectionRadial.Hide();

        m_GazeOver = false;
    }


    private void HandleSelectionComplete()
    {
        // If the user is looking at the rendering of the scene when the radial's selection finishes, activate the button.
        if(m_GazeOver)
            StartCoroutine (ActivateButton());
    }


    private IEnumerator ActivateButton()
    {

        // If the camera is already fading, ignore.
        if (m_CameraFade.IsFading)
            yield break;

        // If anything is subscribed to the OnButtonSelected event, call it.
        if (OnButtonSelected != null)
            OnButtonSelected(this);

        if (OnSelect != null)
            OnSelect();

        //display selector 
        enemySelectorPyramid.SetActive(true);
        enemySelector.attackMenuBtn = this;

        // display confirmation message 
        shownObject.SetActive(true);
        hideShowObjectScript.objectItems = shownObject;
        hideShowObjectScript.objectToShowHide = shownObjectName;
        hideShowObjectScript.Show();

        // hide attack menu
        attackMenu.SetActive(false);
       
        // Wait for the camera to fade out.
        //yield return StartCoroutine(m_CameraFade.BeginFadeOut(true));

        // Load the level.
        //SceneManager.LoadScene(m_SceneToLoad, LoadSceneMode.Single);

        //button selected
        isActivated = true;
        Debug.Log("Attack menu button selected");
    }
}
