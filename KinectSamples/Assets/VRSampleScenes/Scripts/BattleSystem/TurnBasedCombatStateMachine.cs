﻿using UnityEngine;
using System.Collections;

public class TurnBasedCombatStateMachine : MonoBehaviour {
    public GameObject testButton;
    public bool playerCharFainted;
    public bool playerAvesFainted;
    public bool enemyCharFainted;
    public bool enemyAvesFainted;

    /*void OnEnable()
    {
        BattleMenuButton.OnSelect += AdvanceStateMachine;
    }


    void OnDisable()
    {
        BattleMenuButton.OnSelect -= AdvanceStateMachine;
    }*/

    public enum BattleStates
    {
        START,
        PLAYERMOVE1,
        PLAYERMOVE2,
        ENEMYMOVE,
        LOSE,
        WIN,
        CHECKWINLOSE
    }

    public BattleStates currentState;

    void Start()
    {
        currentState = BattleStates.START;
        playerAvesFainted = false;
        playerCharFainted = false;
        enemyCharFainted = false;
        enemyAvesFainted = false;
    }

    void Update()
    {
        //print("BSM update called");  
    }

    public bool inPlayerMove()
    {
        if (currentState == BattleStates.PLAYERMOVE1)
        {
            return true; 
        } else
        {
            return false;
        }
    }

    public bool inEnemyMove()
    {
        if (currentState == BattleStates.ENEMYMOVE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool inCheckWinLose()
    {
        if (currentState == BattleStates.CHECKWINLOSE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool inWinLose()
    {
        if (currentState == BattleStates.WIN || currentState == BattleStates.LOSE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void advanceTo(int stateNum)
    {
        switch(stateNum)
        {
            case (0):
                currentState = BattleStates.START;
                print("advanced to START");
                break;
            case (1):
                if (!playerCharFainted)
                {
                    currentState = BattleStates.PLAYERMOVE1;
                    print("advanced to PLAYERMOVE1");
                } else
                {
                    currentState = BattleStates.PLAYERMOVE2;
                    print("PC fainted; advanced to PLAYERMOVE2");
                }
                break;
            case (2):
                if (!playerAvesFainted)
                {
                    currentState = BattleStates.PLAYERMOVE2;
                    print("advanced to PLAYERMOVE2");
                } else
                {
                    currentState = BattleStates.ENEMYMOVE;
                    print("PA fainted; advanced to ENEMYMOVE");
                }
                break;
            case (3):
                currentState = BattleStates.ENEMYMOVE;
                print("advanced to ENEMYMOVE");
                break;
            case (4):
                currentState = BattleStates.CHECKWINLOSE;
                print("advanced to CHECKWINLOSE");
                break;
            case (5):
                currentState = BattleStates.WIN;
                print("advanced to WIN");
                break;
            case (6):
                currentState = BattleStates.LOSE;
                print("advanced to LOSE");
                break;
        }
    }

    void AdvanceStateMachine()
    {
        print("Advancing State Machine");
        switch (currentState)
        {
            //currently just cycles through the states
            case (BattleStates.START):
                print("START");
                currentState = BattleStates.PLAYERMOVE1;
                print("PLAYERMOVE1");
                break;
            case (BattleStates.PLAYERMOVE2):
                print("PLAYERMOVE2");
                currentState = BattleStates.ENEMYMOVE;
                print("ENEMYMOVE");
                break;
            case (BattleStates.ENEMYMOVE):
                print("ENEMYMOVE");
                currentState = BattleStates.LOSE;
                print("LOSE");
                break;
            case (BattleStates.LOSE):
                print("LOSE");
                currentState = BattleStates.WIN;
                print("WIN");
                break;
            case (BattleStates.WIN):
                print("WIN");
                currentState = BattleStates.START;
                print("START");
                break;
        }
    }    
}