using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;


public class BattleMenuButton : MonoBehaviour
{
    public event Action<BattleMenuButton> OnButtonSelected;                   // This event is triggered when the selection of the button has finished.

    public delegate void Selected();
    public static event Selected OnSelect;
    public HideorShowMenu hideShowMenuScript;
    public GameObject shownObject;
    public string shownObjectName; 
    [SerializeField] private VRCameraFade m_CameraFade;                
    [SerializeField] private SelectionRadial m_SelectionRadial;         // This controls when the selection is complete.
    [SerializeField] private VRInteractiveItem m_InteractiveItem;       // The interactive item for where the user should click to load the level.
    public TurnBasedCombatStateMachine stateMachine;

    private bool m_GazeOver;                                            // Whether the user is looking at the VRInteractiveItem currently.

    public bool isActivated = false; 

    // why does it fall??
    private void Update()
    {
     //   transform.localPosition = new Vector3(0, 0, 0);
    }

    private void OnEnable ()
    {
        m_InteractiveItem.OnOver += HandleOver;
        m_InteractiveItem.OnOut += HandleOut;
        m_SelectionRadial.OnSelectionComplete += HandleSelectionComplete;

        // why would it even have a rigid body?
    /*    Rigidbody rb = GetComponent<Rigidbody>();
        if (rb)
        {
            rb.useGravity = false;
            rb.velocity = new Vector3(0, 0, 0);
        }*/
    }


    private void OnDisable ()
    {
        m_InteractiveItem.OnOver -= HandleOver;
        m_InteractiveItem.OnOut -= HandleOut;
        m_SelectionRadial.OnSelectionComplete -= HandleSelectionComplete;
    }
        

    private void HandleOver()
    {
        // When the user looks at the rendering of the scene, show the radial.
        m_SelectionRadial.Show();

        m_GazeOver = true;
    }


    private void HandleOut()
    {
        // When the user looks away from the rendering of the scene, hide the radial.
        m_SelectionRadial.Hide();

        m_GazeOver = false;
    }


    private void HandleSelectionComplete()
    {
        // If the user is looking at the rendering of the scene when the radial's selection finishes, activate the button.
        if(m_GazeOver)
            StartCoroutine (ActivateButton());
    }


    private IEnumerator ActivateButton()
    {

        // If the camera is already fading, ignore.
        if (m_CameraFade.IsFading)
            yield break;

        // If anything is subscribed to the OnButtonSelected event, call it.
        if (OnButtonSelected != null)
            OnButtonSelected(this);

        if (OnSelect != null)
            OnSelect();

        stateMachine.advanceTo(1);

        hideShowMenuScript.Hide();
        if (shownObject != null)
        {
            if (shownObject.activeSelf == false)
            {
                shownObject.SetActive(true);
            }
            else
            {
                shownObject.SetActive(false);
            }
            hideShowMenuScript.menuToShowHide = shownObjectName;
            hideShowMenuScript.Start();
            hideShowMenuScript.Show();
        }
        

    // Wait for the camera to fade out.
    //yield return StartCoroutine(m_CameraFade.BeginFadeOut(true));

    // Load the level.
    //SceneManager.LoadScene(m_SceneToLoad, LoadSceneMode.Single);

    //button selected
    isActivated = true;
        Debug.Log("Battle menu button selected");
    }
}
