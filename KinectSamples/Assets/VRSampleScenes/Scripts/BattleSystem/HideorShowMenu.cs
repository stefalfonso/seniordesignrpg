﻿using UnityEngine;
using System.Collections;

public class HideorShowMenu : MonoBehaviour {
    public GameObject menuItems;
    public string menuToShowHide; // Takes the string of the menu to show/hide

    private ButtonGestureListener gestureListener;
    public bool isActivated = false;

    public void Hide()
    {
        print("hiding menu");
        MeshRenderer[] lChildRenderers = menuItems.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer lRenderer in lChildRenderers)
        {
            lRenderer.enabled = false;
        }
    }

    public void Show()
    {
        print("showing menu");
        MeshRenderer[] lChildRenderers = menuItems.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer lRenderer in lChildRenderers)
        {
            lRenderer.enabled = true;
        }
    }

    // Use this for initialization
    public void Start () {
	    //menuItems = GameObject.Find(menuToShowHide);
        print("found" + menuItems.ToString());
        // get the gestures listener
        gestureListener = ButtonGestureListener.Instance;
    }
	
	// Update is called once per frame
	void Update () {
        //print("UPDATE");
        //Kinect gesture detection
        if (!gestureListener)
        {
            print("listener did not exist");
            //return;
        }

        if (gestureListener.IsSwipeDown())
        {
            MeshRenderer[] lChildRenderers = menuItems.GetComponentsInChildren<MeshRenderer>();
            if (lChildRenderers[0].enabled == true)
            {
                Hide();
            }
        }

        if (gestureListener.IsSwipeUp())
        {
            MeshRenderer[] lChildRenderers = menuItems.GetComponentsInChildren<MeshRenderer>();
            if (lChildRenderers[0].enabled == false)
            {
                Show();
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            // show
            // renderer.enabled = true;
            //gameObject.GetComponent<Renderer>().enabled = true;

            MeshRenderer[] lChildRenderers = menuItems.GetComponentsInChildren<MeshRenderer>();
            if (lChildRenderers[0].enabled == true)
            {
                Hide();
            } else
            {
                Show();
            }
        }
    }
}
