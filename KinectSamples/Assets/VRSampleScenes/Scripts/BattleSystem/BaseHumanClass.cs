﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BaseHumanClass : BaseCharacterClass
{
    public BaseHumanClass()
    {
        characterType = "Human";
        characterDescription = "Sack of flesh and bones. Strong but occasionally somewhat stupid.";
        attack = 14;
        defense = 10;
        speed = 10;
        MP = 10;
        HP = 100;
    }

    new public void Start()
    {
        //stat1 = GameObject.Find("PStats").GetComponent<Text>();
    }

    new public void Update()
    {
        stat1.text = "Type: " + characterType + "\n" + 
            "Attack: " + attack + "\n" +
            "Defense: " + defense + "\n" +
            "Speed: " + speed + "\n" +
            "MP: " + MP;

        if (HP > 0)
        {
            HPtext.text = "HP: " + HP + "/100";
        }
    }
}
