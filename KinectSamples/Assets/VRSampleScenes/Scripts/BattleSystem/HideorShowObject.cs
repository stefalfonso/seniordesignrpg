﻿using UnityEngine;
using System.Collections;

public class HideorShowObject : MonoBehaviour {

    public GameObject objectItems;
    public string objectToShowHide; // Takes the string of the menu to show/hide
    public bool isActivated = true;

    // Use this for initialization
    public void Start()
    {
        objectItems = GameObject.Find(objectToShowHide);
        print("found the object" + objectItems.ToString());
        //objectItems.SetActive(false); // disable object
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Hide()
    {
        print("hiding obj");
        MeshRenderer[] lChildRenderers = objectItems.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer lRenderer in lChildRenderers)
        {
            lRenderer.enabled = false;
        }
    }

    public void Show()
    {
        print("showing obj");
        MeshRenderer[] lChildRenderers = objectItems.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer lRenderer in lChildRenderers)
        {
            lRenderer.enabled = true;
        }
    }
}
