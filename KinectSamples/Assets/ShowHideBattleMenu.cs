﻿using UnityEngine;
using System.Collections;

namespace VRStandardAssets.Utils
{
    public class ShowHideBattleMenu : MonoBehaviour
    {
        [SerializeField]
        private VRInput m_VRInput;                     // Reference to the VRInput in order to know when Cancel is pressed.


        private void OnEnable()
        {
            m_VRInput.OnSwipeUpDown += HandleHideShowMenu;
        }


        private void OnDisable()
        {
            m_VRInput.OnSwipeUpDown -= HandleHideShowMenu;
        }

        private void HandleHideShowMenu()
        {

        }

    }
}
