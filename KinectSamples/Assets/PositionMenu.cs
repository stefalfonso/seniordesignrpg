﻿using UnityEngine;
using System.Collections;


public class PositionMenu : MonoBehaviour {

    public Camera mainCamera; // we want to put menu in camera space

    // Use this for initialization
    void Start () {
        this.gameObject.SetActive(false);
        // not sure why it would even have a rigidbody, but getting weird gravity effects so ok...

    }

    // if ever unparent from camera, uncomment this stuff
    void positionInFrontOfCamera()
    {
        //this.transform.position = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 10));
        //this.transform.rotation = mainCamera.transform.rotation; // TO DO
     //   print("camera's y position is " + mainCamera.transform.position.y);
    }

    public void Display()
    {
        this.gameObject.SetActive(true);
        // TO-DO freeze everything
        Time.timeScale = 0;
        // when decision happens, load scene or unpause
        positionInFrontOfCamera();
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
        Time.timeScale = 1;
    }
	
	// Update is called once per frame
	void Update () { 
        positionInFrontOfCamera();
        print("UPDATE");
        print("position is " + transform.position);
       
	}
}
